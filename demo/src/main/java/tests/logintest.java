package tests;

import io.github.bonigarcia.wdm.WebDriverManager;
import java.util.concurrent.TimeUnit;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class logintest {
    //static String usuarios[] = {"ESYS01CW1"};r8d1kjA76; rwadmin
    static String usuarios[] = {"Admin", "Admin", "Admin", "Admin", "Admin"};
    static String password = "admin123";

    public static void main(String[] args) {
        WebDriverManager.chromedriver().setup();
        WebDriver driver = new ChromeDriver();
        driver.get("https://opensource-demo.orangehrmlive.com");
        driver.manage().window().maximize();
        WebDriverWait wait = new WebDriverWait(driver, 100);
            //Start for bucle to try with array users and passwords ---------------------------
            for (int i = 0; i < usuarios.length; i++) {
                //User box and Password box, submit button ---------------------------------------
                wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//input[@id='txtUsername']"))).sendKeys(usuarios[i]);
                wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//input[@id='txtPassword']"))).sendKeys(password);
                wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//input[@id='btnLogin']"))).click();                                              
                driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
                driver.findElement(By.xpath("//*[@id='menu_maintenance_purgeEmployee']")).click();
                //Logout botton------------------------------------------------------------------
                driver.findElement(By.xpath("//*[@id='welcome']")).click();
                //Yes botton---------------------------------------------------------------------
                driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
                wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//*[@id='welcome-menu']/ul/li[3]/a")));
                driver.findElement(By.xpath("//*[@id='welcome-menu']/ul/li[3]/a")).click();
            }
    
            driver.quit();
    
}
}
